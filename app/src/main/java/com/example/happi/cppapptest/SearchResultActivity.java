package com.example.happi.cppapptest;

import android.content.Intent;
import android.media.ExifInterface;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class SearchResultActivity extends AppCompatActivity
{
	private static final String LOG_TAG = "SearchResultActivity";
	private static final int DIRTY = 1;
	private static final int CLEAN = 0;

	private File[] _imageList;
	private int[] _imageListDirtyIndices;

	private ListView _imageSearchListView;

	String _data_date;
	String _data_comment;
	String _data_lat_min;
	String _data_lat_max;
	String _data_long_min;
	String _data_long_max;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_result);

		getScreenElements();
		getDataFromIntent();
		getSearchResults();
	}

	private void getScreenElements()
	{
		_imageSearchListView = findViewById( R.id.imageSearchListView );
	}

	private void getSearchResults()
	{
		getAllImagesFromDirectory();

		filterTimeStamp();
		filterComments();

		publishList();
	}

	private void getDataFromIntent()
	{
		Intent imageSearchIntent = getIntent();
		_data_date = imageSearchIntent.getStringExtra( SearchFragment.DATA_DATE );
		_data_comment = imageSearchIntent.getStringExtra( SearchFragment.DATA_COMMENTS );
		_data_lat_min = imageSearchIntent.getStringExtra( SearchFragment.DATA_LAT_MIN );
		_data_lat_max = imageSearchIntent.getStringExtra( SearchFragment.DATA_LAT_MAX );
		_data_long_min = imageSearchIntent.getStringExtra( SearchFragment.DATA_LONG_MIN );
		_data_long_max = imageSearchIntent.getStringExtra( SearchFragment.DATA_LONG_MAX );
	}

	private ExifInterface getImageMetaData( String imagePath )
	{
		try
		{
			ExifInterface exifInterface = new ExifInterface( imagePath );
			return exifInterface;
		}
		catch( IOException e )
		{
			Log.d( LOG_TAG, "unable to open exif image" );
			return null;
		}
	}

	private void filterImageSearchList( File[] list, int[] indices, String data, String exifTag )
	{
		ArrayList<File> resultList = new ArrayList<File>();
		int ii = 0;
		while ( ii < list.length )
		{
			ExifInterface exifInterface = getImageMetaData( list[ii].getPath() );
			if ( !( exifInterface.getAttribute( exifTag ).equals( data ) ) )
			{
				indices[ii] = CLEAN;
			}

			ii++;
		}
	}

	private void getAllImagesFromDirectory()
	{
		_imageList = this.getExternalFilesDir( Environment.DIRECTORY_PICTURES ).listFiles();
		_imageListDirtyIndices = new int[_imageList.length];
		if ( _imageListDirtyIndices != null )
		{
			for ( int ii = 0; ii < _imageListDirtyIndices.length; ii++ )
			{
				_imageListDirtyIndices[ii] = DIRTY;
			}
		}
	}

	private void filterTimeStamp()
	{
		if ( MainSnapFragment.stringIsValid( _data_date ) )
		{
			filterImageSearchList( _imageList, _imageListDirtyIndices, _data_date, ExifInterface.TAG_IMAGE_UNIQUE_ID );
		}
	}

	private void filterComments()
	{
		if ( MainSnapFragment.stringIsValid( _data_comment ) )
		{
			filterImageSearchList( _imageList, _imageListDirtyIndices, _data_comment, ExifInterface.TAG_IMAGE_DESCRIPTION );
		}
	}

	/*private void filterLocation()
	{
		if ( MainSnapFragment.stringIsValid( _data_comment ) )
		{
			filterImageSearchList( _imageList, _imageListDirtyIndices, _data_lat_min, ExifInterface.TAG_GPS_LATITUDE );
		}
	}*/

	private void publishList()
	{
		//_imageSearchListView.addHeaderView(  );
		ArrayList<SearchResultRowStruct> rowDataList = new ArrayList<SearchResultRowStruct>();
		int ii = 0;
		while ( ii < _imageList.length )
		{
			if ( _imageListDirtyIndices[ii] == DIRTY )
			{
				rowDataList.add( new SearchResultRowStruct( _imageList[ii].getName(), _imageList[ii].getPath() ) );
			}
			ii++;
		}

		SearchResultRowAdapter adapter = new SearchResultRowAdapter( this, rowDataList );
		_imageSearchListView.setAdapter( adapter );
	}
}
