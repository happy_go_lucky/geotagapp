package com.example.happi.cppapptest;

import android.util.Log;
import android.util.Rational;

public class EXIF_LocationData
{
	private static final String LOG_TAG = "EXIF_LocationData";
	public static final int DEGREES = 0;
	public static final int MINUTES = 1;
	public static final int SECONDS = 2;

	public static final char NORTH = 'N';
	public static final char EAST = 'E';
	public static final char SOUTH = 'S';
	public static final char WEST = 'W';

	private double _latitude_decimalDegrees;
	private double _longitude_decimalDegrees;

	private Rational _latitude_DMS[];
	private Rational _longitude_DMS[];
	private char _lat_direction;
	private char _long_direction;

	public EXIF_LocationData( double latitude, double longitude )
	{
		_latitude_decimalDegrees = latitude;
		_longitude_decimalDegrees = longitude;

		// compute DMS
		decimal_to_DMS();
	}

	public EXIF_LocationData( Rational[] longi, Rational[] lati, char lat_dir, char long_dir )
	{
		_latitude_DMS = new Rational[3];
		_longitude_DMS = new Rational[3];

		int ii = 0;
		while ( ii < 3 )
		{
			_latitude_DMS[ii] = new Rational( lati[ii].getNumerator(), lati[ii].getDenominator() );
			_longitude_DMS[ii] = new Rational( longi[ii].getNumerator(), longi[ii].getDenominator() );
			ii++;
		}

		if ( validateDirection( lat_dir ) )
		{
			_lat_direction = lat_dir;
		}
		else
		{
			Log.d( LOG_TAG, "incorrect latitude direction" );
		}

		if ( validateDirection( long_dir ) )
		{
			_long_direction = long_dir;
		}
		else
		{
			Log.d( LOG_TAG, "incorrect longitude direction" );
		}

		// compute decimal degrees
		DMS_to_decimal();
	}

	public double getLattitudeDecimalDegrees()
	{
		return _latitude_decimalDegrees;
	}

	public double getLongitudeDecimalDegrees()
	{
		return _longitude_decimalDegrees;
	}

	public Rational[] getLattitudeDMS()
	{
		return _latitude_DMS;
	}

	public Rational[] getLongitudeDMS()
	{
		return _longitude_DMS;
	}

	public char getLatitudeDirection()
	{
		return _lat_direction;
	}

	public char getLongitudeDirection()
	{
		return _long_direction;
	}

	private void DMS_to_decimal()
	{
		_latitude_decimalDegrees =  _latitude_DMS[DEGREES].getNumerator() +
				( _latitude_DMS[MINUTES].getNumerator() / 60 ) +
				( _latitude_DMS[SECONDS].getNumerator() / 3600 );

		_longitude_decimalDegrees=  _longitude_DMS[DEGREES].getNumerator() +
				( _longitude_DMS[MINUTES].getNumerator() / 60 ) +
				( _longitude_DMS[SECONDS].getNumerator() / 3600 );

		// fix direction
		computeLatitudeDirection( _lat_direction );
		computeLongitudeDirection( _long_direction );
	}

	private void decimal_to_DMS()
	{
		computeLatitudeDirection( _latitude_decimalDegrees );
		computeLongitudeDirection( _longitude_decimalDegrees );
		normalizeDirections();

		_latitude_DMS = new Rational[3];
		_longitude_DMS = new Rational[3];

		// compute latitude
		int degrees = ( int ) _latitude_decimalDegrees;
		_latitude_DMS[DEGREES] = new Rational( degrees, 1 );

		double minutes = ( _latitude_decimalDegrees - degrees ) * 60;
		_latitude_DMS[MINUTES] = new Rational( ( int ) minutes, 1 );

		double seconds = ( minutes * 60 ) - 60 * minutes;
		_latitude_DMS[SECONDS] = new Rational( ( int ) seconds, 100 );

		// compute longitude
		degrees = ( int ) _longitude_decimalDegrees;
		_longitude_DMS[DEGREES] = new Rational( degrees, 1 );

		minutes = ( _longitude_decimalDegrees - degrees ) * 60;
		_longitude_DMS[MINUTES] = new Rational( ( int ) minutes, 1 );

		seconds = ( minutes * 60 ) - 60 * minutes;
		_longitude_DMS[SECONDS] = new Rational( ( int ) seconds, 100 );
		Log.d( LOG_TAG, "debug test: " + _longitude_DMS[SECONDS].toString() );
	}

	public boolean validateDirection( char dir )
	{
		return dir == SOUTH || dir == NORTH || dir == EAST || dir == WEST;
	}

	private void computeLatitudeDirection( double value )
	{
		_lat_direction = _latitude_decimalDegrees < 0 ? SOUTH : NORTH;
	}

	private void computeLongitudeDirection( double value )
	{
		_long_direction = _longitude_decimalDegrees < 0 ? WEST : EAST;
	}

	private void computeLatitudeDirection( char value )
	{
		_latitude_decimalDegrees = _latitude_decimalDegrees * _long_direction == SOUTH ? -1 : 1;
	}

	private void computeLongitudeDirection( char value )
	{
		_longitude_decimalDegrees = _longitude_decimalDegrees * _long_direction == WEST ? -1 : 1;
	}

	private void normalizeDirections()
	{
		if ( _latitude_decimalDegrees < 0 ) _latitude_decimalDegrees = _latitude_decimalDegrees * -1;
		if ( _longitude_decimalDegrees < 0 ) _longitude_decimalDegrees = _longitude_decimalDegrees * -1;

	}

	public String latitude_URATIONAL_FORMAT()
	{
		// location data has to be converted to URATIONAL formati i.e. unsigned Rational set converted to string of following form
		// "degrees/1,minutes/1,seconds/100"
		// sign represents the direction, therefore, remember to extract the direction from sign.
		String str = String.format( _latitude_DMS[DEGREES].toString() + "," + _latitude_DMS[MINUTES].toString() + "," + _latitude_DMS[SECONDS].toString() );
		return str;
	}

	public String longitude_URATIONAL_FORMAT()
	{
		// location data has to be converted to URATIONAL formati i.e. unsigned Rational set converted to string of following form
		// "degrees/1,minutes/1,seconds/100"
		// sign represents the direction, therefore, remember to extract the direction from sign.
		String str = String.format( _longitude_DMS[DEGREES].toString() + "," + _longitude_DMS[MINUTES].toString() + "," + _longitude_DMS[SECONDS].toString() );
		return str;
	}
}
