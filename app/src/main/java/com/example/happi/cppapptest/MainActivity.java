package com.example.happi.cppapptest;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends FragmentActivity implements BottomNavigationView.OnNavigationItemSelectedListener
{
	public static final int MY_PERMISSIONS_REQUEST_CODE = 1;
	public static final String LOG_TAG = "MainActivity";
	public static final String BACK_STATE_CAM_FRAGMENT = "back_state_cam_fragment";
	public static final String BACK_STATE_SEARCH_FRAGMENT = "back_state_search_fragment";


	private BottomNavigationView _navVIew;
	private FragmentManager _fragmentManager;
	private FragmentTransaction _fragmentTransaction;

    // Used to load the 'native-lib' library on application startup.
    static
	{
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

		_fragmentManager = getSupportFragmentManager();
		_fragmentTransaction = _fragmentManager.beginTransaction();

		_navVIew = findViewById( R.id.navigationView );
		_navVIew.setOnNavigationItemSelectedListener( this );

		setupPermissions();

		// TODO: implement this
		//hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)

		// Example of a call to a native method
        /*TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());*/
    }

	@Override
	public boolean onNavigationItemSelected( @NonNull MenuItem menuItem )
	{
		int itemId = menuItem.getItemId();
		_fragmentTransaction = _fragmentManager.beginTransaction();

		switch ( itemId )
		{
			case R.id.navigation_camera:
				Log.d( LOG_TAG, "Camera Fragment is selected" );
				MainSnapFragment snapFragment = new MainSnapFragment();
				_fragmentTransaction.replace( R.id.fragment_container, snapFragment );
				// donot add this to back stack because it will be the base fragment, we do not want user to go back from here.
				//_fragmentTransaction.addToBackStack( BACK_STATE_CAM_FRAGMENT );
				_fragmentTransaction.commit();
				return true;

			case R.id.navigation_search:
				Log.d( LOG_TAG, "search Fragment is selected" );
				SearchFragment searchFragment = new SearchFragment();
				_fragmentTransaction.replace( R.id.fragment_container, searchFragment );
				_fragmentTransaction.addToBackStack( BACK_STATE_CAM_FRAGMENT );
				_fragmentTransaction.commit();
				return true;

			default:
				Log.d( LOG_TAG, "Undefined behaviour in navigation view" );
				return false;
		}
	}

	private void setupPermissions()
	{
		Log.d( LOG_TAG, "setting up permissions!" );
		String[] permissions = {
			Manifest.permission.READ_EXTERNAL_STORAGE,
			Manifest.permission.WRITE_EXTERNAL_STORAGE,
			Manifest.permission.CAMERA,
			Manifest.permission.ACCESS_FINE_LOCATION,
			Manifest.permission.INTERNET
		};

		boolean recheckPermissions = false;
		int ii= 0;
		while ( ii < permissions.length )
		{
			if ( ActivityCompat.checkSelfPermission( this, permissions[ii] ) != PackageManager.PERMISSION_GRANTED )
			{
				recheckPermissions = true;
				ii = permissions.length;
			}
			ii++;
		}

		if ( recheckPermissions )
		{
			Log.d( LOG_TAG, "permissions are missing!" );
			// Permission is not granted
			ActivityCompat.requestPermissions( this, permissions, MY_PERMISSIONS_REQUEST_CODE );
		}
		else
		{
			Log.d( LOG_TAG, "permissions are already granted!" );
			//set default action
			_navVIew.setSelectedItemId( R.id.navigation_camera );
		}
	}

	public void onRequestPermissionsResult( int requestCode, String permissions[], int[] grantResults )
	{
		switch ( requestCode )
		{
			case MY_PERMISSIONS_REQUEST_CODE:
				// If request is cancelled, the result arrays are empty.
				if ( grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED )
				{
					Log.d( LOG_TAG, "all permissions are good." );
					_navVIew.setSelectedItemId( R.id.navigation_camera );
				}
				else
				{
					// permission denied. Kill the app
					this.finish();
				}
				break;
		}
	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data )
	{
		Log.d( LOG_TAG, "intent result" );
		Fragment frag = _fragmentManager.findFragmentById( R.id.fragment_container );
		frag.onActivityResult( requestCode, resultCode, data );
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		Log.d( LOG_TAG, "activity is paused." );
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Log.d( LOG_TAG, "activity is resumed." );
	}

	@Override
	protected void onDestroy()
	{
		//Fragment frag = _fragmentManager.findFragmentById( R.id.fragment_container );
		//_fragmentTransaction.remove( frag ).commit();
		super.onDestroy();
	}

	/**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
}
