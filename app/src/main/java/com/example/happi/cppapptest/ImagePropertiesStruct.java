package com.example.happi.cppapptest;

public final class ImagePropertiesStruct
{
	private String _imageLatitude;
	private char _imageLatitudeRef;
	private String _imageLongitude;
	private char _imageLongitudeRef;
	private String _imageComment;
	private String _imageTimestamp;
	private String _imageFileName;

	public ImagePropertiesStruct()
	{
		_imageLatitude = "";
		_imageLongitude = "";
		_imageComment = "";
		_imageTimestamp = "";
		_imageFileName = "";
		_imageLatitudeRef = '\0';
		_imageLongitudeRef = '\0';
	}

	public final void setLatitude( String value )
	{
		_imageLatitude = value;
	}

	public final void setLatitudeRef( char value )
	{
		_imageLatitudeRef = value;
	}

	public final void setLongitude( String value )
	{
		_imageLongitude = value;
	}

	public final void setLongitudeRef( char value )
	{
		_imageLongitudeRef = value;
	}

	public final void setComment( String value )
	{
		_imageComment = value;
	}

	public final void setTimeStamp( String value )
	{
		_imageTimestamp = value;
	}

	public final void setFileName( String value )
	{
		_imageFileName = value;
	}

	public final String getLatitude()
	{
		return _imageLatitude;
	}

	public final String getLongitude()
	{
		return _imageLongitude;
	}

	public final String getComment()
	{
		return _imageComment;
	}

	public final String getTimeStamp()
	{
		return _imageTimestamp;
	}

	public final String getFileName()
	{
		return _imageFileName;
	}

	public final char getLatitudeRef()
	{
		return _imageLatitudeRef;
	}

	public final char getLongitudeRef()
	{
		return _imageLongitudeRef;
	}
}