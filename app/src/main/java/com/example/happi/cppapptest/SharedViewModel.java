package com.example.happi.cppapptest;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/***
 *
 * This class lets share data among the fragments in a more effiecient way.
 * It promotes re-usability of fragments so they are bound to an activity
 */

public class SharedViewModel extends ViewModel
{
	private MutableLiveData<Integer> _somedata = new MutableLiveData<Integer>();

	public LiveData<Integer> getData()
	{
		return _somedata;
	}
}
