package com.example.happi.cppapptest;

import android.location.Location;

public interface ICallback
{
    public void callback( Location locData );
}
