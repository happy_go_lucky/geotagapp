package com.example.happi.cppapptest;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import java.text.DateFormat;

import static android.provider.AlarmClock.EXTRA_MESSAGE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SearchFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment implements CompoundButton.OnCheckedChangeListener
{
	public static final String DATA_LAT_MIN = "data_lat_min";
	public static final String DATA_LAT_MAX = "data_lat_max";
	public static final String DATA_LONG_MIN = "data_long_min";
	public static final String DATA_LONG_MAX = "data_long_max";
	public static final String DATA_DATE = "data_date";
	public static final String DATA_COMMENTS = "data_comments";

	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";

	private static final String LOG_TAG = "SearchFragment";

	// TODO: Rename and change types of parameters
	private String mParam1;
	private String mParam2;

	private View _view;
	private InputMethodManager _inputManager;

	private Switch _switch_date;
	private Switch _switch_comment;
	private Switch _switch_location;

	private EditText _latitudeMinInput;
	private EditText _latitudeMaxInput;
	private EditText _longitudeMaxInput;
	private EditText _longitudeMinInput;

	private EditText _dateInput;
	private EditText _commentInput;
	private ImageButton _searchButton;

	public SearchFragment()
	{
		// Required empty public constructor
	}

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param param1 Parameter 1.
	 * @param param2 Parameter 2.
	 * @return A new instance of fragment SearchFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static SearchFragment newInstance( String param1, String param2 )
	{
		SearchFragment fragment = new SearchFragment();
		Bundle args = new Bundle();
		args.putString( ARG_PARAM1, param1 );
		args.putString( ARG_PARAM2, param2 );
		fragment.setArguments( args );
		return fragment;
	}

	@Override
	public void onCreate( Bundle savedInstanceState )
	{
		super.onCreate( savedInstanceState );
		if ( getArguments() != null )
		{
			mParam1 = getArguments().getString( ARG_PARAM1 );
			mParam2 = getArguments().getString( ARG_PARAM2 );
		}
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		// Inflate the layout for this fragment
		return inflater.inflate( R.layout.fragment_search, container, false );
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed( Uri uri )
	{

	}

	@Override
	public void onAttach( Context context )
	{
		super.onAttach( context );
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener
	{
		// TODO: Update argument type and name
		void onFragmentInteraction( Uri uri );
	}

	@Override
	public void onResume()
	{
		super.onResume();
		getScreenElements();
		setupScreenElements();
	}

	private void getScreenElements()
	{
		_view = getView();
		_inputManager = ( InputMethodManager ) getActivity().getSystemService( Context.INPUT_METHOD_SERVICE );

		_switch_date = _view.findViewById( R.id.switchDate );
		_switch_location = _view.findViewById( R.id.switchLocation );
		_switch_comment = _view.findViewById( R.id.switchComment );

		_switch_date.setOnCheckedChangeListener( this );
		_switch_location.setOnCheckedChangeListener( this );
		_switch_comment.setOnCheckedChangeListener( this );

		_latitudeMinInput = _view.findViewById( R.id.latMinInput );
		_latitudeMaxInput = _view.findViewById( R.id.latMaxInput );
		_longitudeMinInput = _view.findViewById( R.id.longMinInput );
		_longitudeMaxInput = _view.findViewById( R.id.longMaxInput );

		_dateInput = _view.findViewById( R.id.dateInput );
		_commentInput = _view.findViewById( R.id.commentInput );

		_searchButton = _view.findViewById( R.id.btn_searchImages );
	}

	private void setupScreenElements()
	{
		_switch_date.setChecked( false );
		_switch_location.setChecked( false );
		_switch_comment.setChecked( false );

		// TODO: this should be triggered by setChecked, but for some unknown reasons it does not here
		toggleDateSectionFocus( _switch_date.isChecked() );
		toggleCommentSectionFocus( _switch_comment.isChecked() );
		toggleLocationSectionFocus( _switch_location.isChecked() );

		// setup the virtual keyboard next button action
		_dateInput.setImeOptions( EditorInfo.IME_ACTION_DONE );
		_commentInput.setImeOptions( EditorInfo.IME_ACTION_DONE );
		_latitudeMinInput.setImeOptions( EditorInfo.IME_ACTION_DONE );
		_latitudeMaxInput.setImeOptions( EditorInfo.IME_ACTION_DONE );
		_longitudeMaxInput.setImeOptions( EditorInfo.IME_ACTION_DONE );
		_longitudeMinInput.setImeOptions( EditorInfo.IME_ACTION_DONE );

		_searchButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick( View v )
			{
				onSearchButtonClick( v );
			}
		} );
	}

	@Override
	public void onCheckedChanged( CompoundButton compoundButton, boolean b )
	{
		Log.d( LOG_TAG, "onCheckedChanged" );
		hideKeyboard();
		switch( compoundButton.getId() )
		{
			case R.id.switchDate:
				toggleDateSectionFocus( _switch_date.isChecked() );
				break;

			case R.id.switchLocation:
				toggleLocationSectionFocus( _switch_location.isChecked() );
				break;

			case R.id.switchComment:
				toggleCommentSectionFocus( _switch_comment.isChecked() );
				break;

			default:
				Log.d( LOG_TAG, "invalid button id found." );
				break;
		}
	}

	private void toggleDateSectionFocus( boolean isActive )
	{
		if ( isActive )
		{
			_dateInput.setInputType( InputType.TYPE_CLASS_DATETIME );
		}
		else
		{
			_dateInput.setInputType( InputType.TYPE_NULL );
			clearDateInput();
		}

		setDateInputFocus( isActive );
	}

	private void toggleCommentSectionFocus( boolean isActive )
	{
		if ( isActive )
		{
			_commentInput.setInputType( InputType.TYPE_CLASS_TEXT );
		}
		else
		{
			_commentInput.setInputType( InputType.TYPE_NULL );
			clearCommentInput();
		}

		setCommentInputFocus( isActive );
	}

	private void toggleLocationSectionFocus( boolean isActive )
	{
		Log.d( LOG_TAG, "toggleLocationSectionFocus" );
		if ( isActive )
		{
			_latitudeMinInput.setInputType( InputType.TYPE_NUMBER_FLAG_DECIMAL );
			_latitudeMaxInput.setInputType( InputType.TYPE_NUMBER_FLAG_DECIMAL );
			_longitudeMaxInput.setInputType( InputType.TYPE_NUMBER_FLAG_DECIMAL );
			_longitudeMinInput.setInputType( InputType.TYPE_NUMBER_FLAG_DECIMAL );
		}
		else
		{
			_latitudeMinInput.setInputType( InputType.TYPE_NULL );
			_latitudeMaxInput.setInputType( InputType.TYPE_NULL );
			_longitudeMaxInput.setInputType( InputType.TYPE_NULL );
			_longitudeMinInput.setInputType( InputType.TYPE_NULL );

			clearLocationInput();
		}

		setLocationInputFocus( isActive );
	}

	private void hideKeyboard()
	{
		if ( _view != null )
		{
			_inputManager.hideSoftInputFromWindow( _view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS );
		}
	}

	private void clearLocationInput()
	{
		_latitudeMinInput.setText("");
		_latitudeMaxInput.setText("");
		_longitudeMaxInput.setText("");
		_longitudeMinInput.setText("");
	}

	private void clearDateInput()
	{
		_dateInput.setText("");
	}

	private void clearCommentInput()
	{
		_commentInput.setText("");
	}

	private void setLocationInputFocus( boolean enable )
	{
		_latitudeMinInput.setFocusable( enable );
		_latitudeMaxInput.setFocusable( enable );
		_longitudeMaxInput.setFocusable( enable );
		_longitudeMinInput.setFocusable( enable );

		_latitudeMinInput.setFocusableInTouchMode( enable );
		_latitudeMaxInput.setFocusableInTouchMode( enable );
		_longitudeMaxInput.setFocusableInTouchMode( enable );
		_longitudeMinInput.setFocusableInTouchMode( enable );
	}

	private void setDateInputFocus( boolean enable )
	{
		_dateInput.setFocusable( enable );
		_dateInput.setFocusableInTouchMode( enable );
	}

	private void setCommentInputFocus( boolean enable )
	{
		_commentInput.setFocusable( enable );
		_commentInput.setFocusableInTouchMode( enable );
	}

	private void onSearchButtonClick( View v )
	{
		Intent searchResultsIntent = new Intent( getContext(), SearchResultActivity.class );

		searchResultsIntent.putExtra( DATA_LAT_MIN, _latitudeMinInput.getText().toString() );
		searchResultsIntent.putExtra( DATA_LAT_MAX, _latitudeMaxInput.getText().toString() );
		searchResultsIntent.putExtra( DATA_LONG_MIN, _longitudeMinInput.getText().toString() );
		searchResultsIntent.putExtra( DATA_LONG_MAX, _longitudeMaxInput.getText().toString() );
		searchResultsIntent.putExtra( DATA_DATE, _dateInput.getText().toString() );
		searchResultsIntent.putExtra( DATA_COMMENTS, _commentInput.getText().toString() );

		startActivity( searchResultsIntent );
	}
}
