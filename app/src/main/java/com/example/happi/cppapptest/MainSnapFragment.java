package com.example.happi.cppapptest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Rational;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainSnapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainSnapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainSnapFragment extends Fragment implements ICallback
{
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
	private static final String ARG_PARAM1 = "param1";
	private static final String ARG_PARAM2 = "param2";

	// TODO: Rename and change types of parameters
	private String mParam1;
	private String mParam2;

	public static String IMAGE_NAME = "GeoTagImage";
	public static String NO_COMMENT_MESSAGE = "Enter a comment";
	public static String NO_TIMESTAMP_MESSAGE = "No timestamp available";
	public static String TIMESTAMP_PATTERN = "yyyyMMdd_HHmmss";
	public static String IMAGE_EXTENSION = ".jpeg";
	public static final String EXTERNAL_VOLUME_NAME = "external";
	public static final String INTERNAL_VOLUME_NAME = "internal";
	public static final String LOG_TAG = "MainSnapFragment";
	public static final int TAKE_A_PICTURE = 1;

	private ImageView _imageView;
	private ImageButton _cameraButton;
	private ImageButton _leftNavButton;
	private ImageButton _rightNavButton;
	private ImageButton _saveCommentButton;
	private TextView _timeStampTextView;
	private EditText _commentTextView;

	private Uri _imageURI;
	private View _view;
	private File[] _imageList;
	private int _imageViewImageIndex;
	private Location _locData = null;
	private SharedViewModel _sharedViewModelData;
	private File _imageCapturedFileName;
	private String _imageTimeStamp;
	private String _imageComment;


	public MainSnapFragment()
	{
		// Required empty public constructor
	}

	/**
	 * Use this factory method to create a new instance of
	 * this fragment using the provided parameters.
	 *
	 * @param param1 Parameter 1.
	 * @param param2 Parameter 2.
	 * @return A new instance of fragment MainSnapFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static MainSnapFragment newInstance( String param1, String param2 )
	{
		MainSnapFragment fragment = new MainSnapFragment();
		Bundle args = new Bundle();
		args.putString( ARG_PARAM1, param1 );
		args.putString( ARG_PARAM2, param2 );
		fragment.setArguments( args );
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if ( getArguments() != null )
		{
			mParam1 = getArguments().getString( ARG_PARAM1 );
			mParam2 = getArguments().getString( ARG_PARAM2 );
		}

		initLocalVaiables();
		setupLocationData();
	}

	@Override
	public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState )
	{
		// Inflate the layout for this fragment
		return inflater.inflate( R.layout.fragment_main_snap, container, false );
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		_view = getView();
		getScreenElements();
		setupScreenElements();
		updateImageList();
		updateImageView( _imageViewImageIndex );
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed( Uri uri )
	{

	}

	@Override
	public void onAttach( Context context )
	{
		super.onAttach( context );
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated
	 * to the activity and potentially other fragments contained in that
	 * activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener
	{
		// TODO: Update argument type and name
		void onFragmentInteraction( Uri uri );
	}

	private void initLocalVaiables()
	{
		_imageTimeStamp = "";
		_imageComment = "";
	}

	private void getScreenElements()
	{
		Log.d( LOG_TAG, "getScreenElements" );
		_imageView = _view.findViewById( R.id.snapImageView );
		_cameraButton = _view.findViewById( R.id.btn_camera );
		_leftNavButton = _view.findViewById( R.id.btn_nav_left );
		_rightNavButton = _view.findViewById( R.id.btn_nav_right );
		_timeStampTextView = _view.findViewById( R.id.timeStampText );
		_commentTextView = _view.findViewById( R.id.commentText );
		_saveCommentButton = _view.findViewById( R.id.btn_save_comment );
	}

	private void setupScreenElements()
	{
		Log.d( LOG_TAG, "setupScreenElements" + _cameraButton.toString() );


		_cameraButton.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View v )
			{
				onCameraButtonClick( v );
			}
		} );

		_leftNavButton.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View v )
			{
				onLeftNavButtonClick();
			}
		} );

		_rightNavButton.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View v )
			{
				onRightNavButtonClick();
			}
		} );
		_saveCommentButton.setOnClickListener( new View.OnClickListener()
		{
			@Override
			public void onClick( View v )
			{
				saveCommentToImage( _commentTextView.getText().toString() );
			}
		} );
	}

	public void updateImageList()
	{
		_imageList = _view.getContext().getExternalFilesDir( Environment.DIRECTORY_PICTURES ).listFiles();
		_imageViewImageIndex = _imageList.length - 1;
	}

	public void updateImageView( int imageIndex )
	{
		if ( isImageListValid() )
		{
			if ( imageIndex < 0 )
			{
				imageIndex = _imageViewImageIndex;
			}
			_imageView.setImageBitmap( BitmapFactory.decodeFile( _imageList[imageIndex].getPath() ) );
		}
		else
		{
			Log.d( LOG_TAG, "You don't have any pictures in Gallery. Please get some pictures to be loaded." );
			alertUser( R.string.snapFragmentAlertMessage, R.string.snapFragmentAlertTitle );
		}
		updateTimeStamp();
		updateCommentTextView();
	}

	private void alertUser( int Message, int title )
	{
		// 1. Instantiate an AlertDialog.Builder with its constructor
		AlertDialog.Builder builder = new AlertDialog.Builder( getActivity() );

		// 2. Chain together various setter methods to set the dialog characteristics
		builder.setMessage( Message ).setTitle( title );
		builder.setPositiveButton( R.string.button_string_ok, new DialogInterface.OnClickListener()
		{
			public void onClick(DialogInterface dialog, int id)
			{
				// User clicked OK button
			}
		});

		// 3. Get the AlertDialog from create()
		AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void onCameraButtonClick( View view )
	{
		launchCamera();
	}

	private void launchCamera()
	{
		Log.d( LOG_TAG, "launching camera.." );
		Intent cameraOpenIntent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );

		_imageCapturedFileName = setupImageParameters();

		if ( cameraOpenIntent.resolveActivity( _view.getContext().getPackageManager() ) != null && _imageCapturedFileName != null )
		{
			_imageURI = FileProvider.getUriForFile( _view.getContext(), "com.example.happi.cppapptest.fileprovider", _imageCapturedFileName );
			cameraOpenIntent.putExtra( MediaStore.EXTRA_OUTPUT, _imageURI );

			getActivity().startActivityForResult( cameraOpenIntent, TAKE_A_PICTURE );
		}
	}

	private File setupImageParameters()
	{
		Log.d( LOG_TAG, "setupImageParameters" );
		File fileToSaveTo = null;
		_imageTimeStamp = new SimpleDateFormat( TIMESTAMP_PATTERN ).format( new Date() );

		if ( Environment.getExternalStorageState().equals( Environment.MEDIA_MOUNTED ) )
		{
			String filename = IMAGE_NAME + _imageTimeStamp;

			File storageDir = _view.getContext().getExternalFilesDir( Environment.DIRECTORY_PICTURES );
			try
			{
				fileToSaveTo = File.createTempFile( filename, IMAGE_EXTENSION, storageDir );
			}
			catch( Exception e )
			{
				// TODO: do something
			}
		}
		else
		{
			// TODO: external media is not mounted; do something about it.
			Log.d( LOG_TAG, "external media is not mounted." );
		}

		return fileToSaveTo;
	}

	private void onLeftNavButtonClick()
	{
		if ( !isImageListValid() ) return;

		_imageViewImageIndex--;

		if ( _imageViewImageIndex < 0 && _imageList.length >= 0 )
		{
			_imageViewImageIndex = _imageList.length - 1;
		}

		updateImageView( _imageViewImageIndex );
	}

	private void onRightNavButtonClick()
	{
		if ( !isImageListValid() ) return;

		_imageViewImageIndex++;

		if ( _imageViewImageIndex >= _imageList.length )
		{
			_imageViewImageIndex = 0;
		}
		updateImageView( _imageViewImageIndex );
	}

	private boolean isImageListValid()
	{
		return _imageList.length > 0;
	}

	private void setupLocationData()
	{
		LocationData locData = new LocationData( this, getContext() );
		String providerName = locData.getProvider();
		_locData = locData.getExistingLocData();

		if ( providerName == null )
		{
			promptEnableLocationSettings();
		}
		else
		{
			locData.registerLocationUpdate( providerName );
		}
	}

	public void callback( Location locData )
	{
		_locData = locData;
	}

	public Location getLocData()
	{
		return _locData;
	}

	private void promptEnableLocationSettings()
	{
		Intent settingsIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS );
		startActivity( settingsIntent );
	}

	private void addExifTags( ImagePropertiesStruct imageProperties )
	{
		Log.d( LOG_TAG, "URATIONAL lat: " + imageProperties.getLatitude() );
		Log.d( LOG_TAG, "URATIONAL long: " + imageProperties.getLongitude() );

		ExifInterface exifInterface = getImageMetaData( imageProperties.getFileName() );
		if ( exifInterface != null )
		{
			// coordinates
			exifInterface.setAttribute( ExifInterface.TAG_GPS_LATITUDE, imageProperties.getLatitude() );
			exifInterface.setAttribute( ExifInterface.TAG_GPS_LONGITUDE, imageProperties.getLongitude() );

			// direction
			exifInterface.setAttribute( ExifInterface.TAG_GPS_LATITUDE_REF, "" + imageProperties.getLatitudeRef() );
			exifInterface.setAttribute( ExifInterface.TAG_GPS_LONGITUDE_REF, "" + imageProperties.getLongitudeRef() );

			// meta data
			exifInterface.setAttribute( ExifInterface.TAG_IMAGE_DESCRIPTION, imageProperties.getComment() );
			exifInterface.setAttribute( ExifInterface.TAG_IMAGE_UNIQUE_ID, imageProperties.getTimeStamp() );
			try
			{
				exifInterface.saveAttributes();
			}
			catch ( IOException e )
			{
				Log.d( LOG_TAG, "failed to save image metadata" );
			}

			// verification
			Log.d( LOG_TAG, "image latitude: " + exifInterface.getAttribute( ExifInterface.TAG_GPS_LATITUDE ) );
			Log.d( LOG_TAG, "image longitude: " + exifInterface.getAttribute( ExifInterface.TAG_GPS_LONGITUDE ) );
			Log.d( LOG_TAG, "image comment: " + exifInterface.getAttribute( ExifInterface.TAG_IMAGE_DESCRIPTION ) );
			Log.d( LOG_TAG, "image timestamp: " + exifInterface.getAttribute( ExifInterface.TAG_IMAGE_UNIQUE_ID ) );
		}
	}

	private void updateImageExifTags()
	{
		double longitude = getLocData().getLongitude();
		double latitude = getLocData().getLatitude();
		Log.d( LOG_TAG, latitude+ ", " + longitude );

		EXIF_LocationData exif_locData = new EXIF_LocationData( latitude, longitude );

		ImagePropertiesStruct imageProperties = new ImagePropertiesStruct();
		imageProperties.setLongitude( exif_locData.longitude_URATIONAL_FORMAT() );
		imageProperties.setLatitude( exif_locData.latitude_URATIONAL_FORMAT() );
		imageProperties.setLatitudeRef( exif_locData.getLatitudeDirection() );
		imageProperties.setLongitudeRef( exif_locData.getLongitudeDirection() );
		imageProperties.setComment( "" );
		imageProperties.setTimeStamp( _imageTimeStamp );
		imageProperties.setFileName( _imageCapturedFileName.getPath() );

		addExifTags( imageProperties );
	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data )
	{
		if ( requestCode == TAKE_A_PICTURE )
		{
			if (resultCode == Activity.RESULT_OK)
			{
				updateImageExifTags();
				updateImageList();
				updateImageView( -1 );
			}
			else
			{
				// TODO: something went wrong. Do something
				Log.d(LOG_TAG, "camera couldn't take the picture");
			}
		}
	}

	@Override
	public void onResume()
	{
		Log.d( LOG_TAG, "fragment is resumed." );
		super.onResume();

		//updateImageList();
		//updateImageView( -1 );
	}

	private void updateTimeStamp()
	{
		if ( isImageListValid() )
		{
			ExifInterface exifInterface = getImageMetaData( _imageList[_imageViewImageIndex].getPath() );
			if ( exifInterface != null )
			{
				_imageTimeStamp = exifInterface.getAttribute( ExifInterface.TAG_IMAGE_UNIQUE_ID );
			}
			else
			{
				_imageTimeStamp = "";
			}
		}

		setTimestampTextValue( _imageTimeStamp );
	}

	private void updateCommentTextView()
	{
		if ( isImageListValid() )
		{
			ExifInterface exifInterface = getImageMetaData( _imageList[_imageViewImageIndex].getPath() );
			if ( exifInterface != null )
			{
				_imageComment = exifInterface.getAttribute( ExifInterface.TAG_IMAGE_DESCRIPTION );
			}
			else
			{
				_imageComment = "";
			}
		}

		setCommentTextValue( _imageComment );
	}

	private void saveCommentToImage( String comment )
	{
		if ( isImageListValid() )
		{
			ExifInterface exifInterface = getImageMetaData( _imageList[_imageViewImageIndex].getPath() );
			if ( exifInterface != null )
			{
				exifInterface.setAttribute( ExifInterface.TAG_IMAGE_DESCRIPTION, comment );
				try
				{
					exifInterface.saveAttributes();
				}
				catch( IOException e )
				{
					Log.d( LOG_TAG, "failed to save image meta data" );
				}
			}
		}
	}

	public static boolean stringIsValid( String str )
	{
		return !( str == null || str.equals( "" ) );
	}

	private ExifInterface getImageMetaData( String imagePath )
	{
		try
		{
			ExifInterface exifInterface = new ExifInterface( imagePath );
			return exifInterface;
		}
		catch( IOException e )
		{
			Log.d( LOG_TAG, "unable to open exif image" );
			return null;
		}
	}

	private void setCommentTextValue( String value )
	{
		if ( stringIsValid( value ) )
		{
			char[] comment = value.toCharArray();
			_commentTextView.setText( comment, 0, comment.length );
		}
		else
		{
			_commentTextView.setText( NO_COMMENT_MESSAGE );
		}
	}

	private void setTimestampTextValue( String value )
	{
		if ( stringIsValid( value ) )
		{
			char[] timestamp = value.toCharArray();
			_timeStampTextView.setText( timestamp, 0, timestamp.length );
		}
		else
		{
			_timeStampTextView.setText( NO_TIMESTAMP_MESSAGE );
		}
	}
}