package com.example.happi.cppapptest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchResultRowAdapter extends BaseAdapter
{
	// use this if you need different views for different situations
	private static final int NUM_VIEWS = 1;

	private static final int IMAGE_SIZE = 128;
	private static final String LOG_TAG = "SearchResultRowAdapter";

	Context _context;
	ArrayList<SearchResultRowStruct> _rowItemList;

	public SearchResultRowAdapter( Context context, ArrayList<SearchResultRowStruct> rowItemList )
	{
		_context = context;
		_rowItemList = rowItemList;
	}

	@Override
	public Object getItem( int currentItemIndex )
	{
		return _rowItemList.get( currentItemIndex );
	}

	@Override
	public long getItemId( int currentItemIndex )
	{
		return currentItemIndex;
	}

	@Override
	public int getCount()
	{
		return _rowItemList.size();
	}

	@Override
	public View getView( int currentItemIndex, View view, ViewGroup viewGroup )
	{
		// inflate the layout for each row
		if ( view == null )
		{
			view = LayoutInflater.from( _context ).inflate( R.layout.search_result_row, viewGroup, false );
		}

		// update the view children
		ImageView imageView = view.findViewById( R.id.listViewBitmapImage );
		TextView imageDescription = view.findViewById( R.id.listViewImageName );

		// update the children
		Bitmap imageBMP = BitmapFactory.decodeFile( ( ( SearchResultRowStruct ) getItem( currentItemIndex ) ).getImagePath() );
		imageView.setImageBitmap( ThumbnailUtils.extractThumbnail( imageBMP, IMAGE_SIZE, IMAGE_SIZE ) );
		imageDescription.setText( ( ( SearchResultRowStruct ) getItem( currentItemIndex ) ).getImageName() );

		return view;
	}

	@Override
	public int getViewTypeCount()
	{
		return NUM_VIEWS;
	}
}
