package com.example.happi.cppapptest;

public class SearchResultRowStruct
{
	String _imageName;
	String _imagePath;

	public SearchResultRowStruct( String imageName, String imagePath )
	{
		_imageName = imageName;
		_imagePath = imagePath;
	}

	public String getImageName()
	{
		return _imageName;
	}

	public String getImagePath()
	{
		return _imagePath;
	}

	public void setImageName( String value )
	{
		_imageName = value;
	}

	public void setImagePath( String value )
	{
		_imagePath = value;
	}
}
