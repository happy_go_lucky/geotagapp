package com.example.happi.cppapptest;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Rational;

public class LocationData
{
	public static final String LOG_TAG = "LocationData";

	private LocationManager _locManager;
	private Criteria _locationAccuracyCriteria;
	private Location _locData;
	private Context _appContext;
	private ICallback _locdataUpdateCallback;

	public LocationData( ICallback locDataCallback, Context context )
	{
		_appContext = context;
		_locdataUpdateCallback = locDataCallback;
		_locManager = ( LocationManager ) _appContext.getSystemService( Context.LOCATION_SERVICE );

		try
		{
			_locData = _locManager.getLastKnownLocation( LocationManager.GPS_PROVIDER );
		}
		catch ( SecurityException e )
		{
			Log.d( LOG_TAG, "permissions are not granted!" );
			// TODO: display a message to user.
		}
		_locationAccuracyCriteria = new Criteria();
		_locationAccuracyCriteria.setAccuracy( Criteria.ACCURACY_FINE );
		_locationAccuracyCriteria.setPowerRequirement( Criteria.POWER_LOW );
		_locationAccuracyCriteria.setAltitudeRequired( false );
		_locationAccuracyCriteria.setSpeedRequired( false );
		_locationAccuracyCriteria.setBearingRequired( false );	// Bearing is direction
		_locationAccuracyCriteria.setCostAllowed( false );
		_locationAccuracyCriteria.setHorizontalAccuracy( Criteria.ACCURACY_FINE );
		_locationAccuracyCriteria.setVerticalAccuracy( Criteria.ACCURACY_LOW );

		boolean is_GPS_Enabled = _locManager.isProviderEnabled( LocationManager.GPS_PROVIDER );
		if ( is_GPS_Enabled )
		{
			// TODO: do something useful
		}
		else
		{
			// TODO: show an alert message here. maybe enable gps somehow
		}
	}

	public String getProvider()
	{
		return _locManager.getBestProvider( _locationAccuracyCriteria, true );
	}

	public Location getExistingLocData()
	{
		return _locData;
	}

	public void registerLocationUpdate( String provider )
	{
		Location locData = new Location( provider );
		LocationListener locListener = new LocationListener() {
			@Override
			public void onLocationChanged( Location location )
			{
				_locData = location;
				_locdataUpdateCallback.callback( location );
			}

			@Override
			public void onStatusChanged( String s, int i, Bundle bundle )
			{

			}

			@Override
			public void onProviderEnabled( String s )
			{

			}

			@Override
			public void onProviderDisabled( String s )
			{

			}
		};

		Looper looper = null;
		try
		{
			// this is good for battery, although location is not very precise
			_locManager.requestSingleUpdate( provider, locListener, looper );
		}
		catch ( SecurityException e )
		{
			Log.d( LOG_TAG, "permissions are not granted!" );
			// TODO: display a message to user.
		}
	}
}
