package com.example.happi.cppapptest;

import android.util.Log;
import android.view.View;

import androidx.test.espresso.NoMatchingRootException;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.view.MotionEvent.BUTTON_PRIMARY;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class TestMainActivity
{
	@Rule
	public ActivityTestRule<MainActivity> activityActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

	@Before
	public void init()
	{
		activityActivityTestRule.getActivity().getSupportFragmentManager().beginTransaction();
	}

	@Test
	public void commentTest()
	{
		try
		{
			onView( withText("OK") ).check( matches( isDisplayed() ) ).perform( pressBack() );
		}
		catch( NoMatchingRootException e )
		{
			Log.d( "EspressoMainActivity", "alert view not found" );
		}
		onView( withId( R.id.commentText ) ).perform( replaceText( "a comment for this image" ) );
		onView( withId( R.id.commentText ) ).check( matches( withText( "a comment for this image" ) ) );
	}
}
